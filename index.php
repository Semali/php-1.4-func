<?php
error_reporting(E_ALL);
mb_internal_encoding('utf-8');

$pathimg = './img/'; // Директория изображений
$pathcsv = './csv/'; // Директория с csv
$mycsv = '/mycsv.csv';
$fileslist = array_diff(scandir($pathimg), array('..', '.')); // Массив ссылок на файлы изображений + избавился от лишних элементов ('..', '.')

$nw = 150;    // Ширина миниатюр
$nh = 100;    // Высота миниатюр

foreach ($fileslist as $key => $value) {
	// Убираем миниатюры из цикла
	if (substr($value, 0, strpos($value, "_")) == 'thumb') {
		continue;
	}

	echo $value; // Изображение
	echo '<br>';

	echo 'Последнее время изменения: ' . date('F d Y H:i:s;', filemtime($pathimg . $value));
	echo '<br>';

	$fopenr = fopen($pathimg . $value, 'r');
	$w = getimagesize($pathimg . $value)[0];
	$h = getimagesize($pathimg . $value)[1];
	echo 'Размер изображения: ширина: ' . $w . 'px , высота: ' . $h . 'px;';
	fclose($fopenr);
	echo '<br>';

	echo 'Вес файла: ' . filesize($pathimg.$value) . ' byte.';
	echo '<br>';

	$source = $pathimg . $value; // Исходный файл
	$dest = $pathimg . 'thumb_' . $value; // Файл с результатом работ

	$stype = explode('.', $source);
	$stype = $stype[count($stype) - 1]; // Формат файла

	// Выбираем нужный файл
	switch($stype) {
	    case 'gif': $simg = imagecreatefromgif($source);
	    break;
	    case 'jpg': $simg = imagecreatefromjpeg($source);
	    break;
	    case 'png': $simg = imagecreatefrompng($source);
	    break;
	}

	$dimg = imagecreatetruecolor($nw, $nh);
	$wm = $w / $nw;
	$hm = $h / $nh;
	$h_height = $nh / 2;
	$w_height = $nw / 2;

	if($w > $h) {
		$adjusted_width = $w / $hm;
		$half_width = $adjusted_width / 2;
		$int_width = $half_width - $w_height;
		imagecopyresampled($dimg, $simg, -$int_width, 0, 0, 0, $adjusted_width, $nh, $w, $h);
	} elseif(($w < $h) || ($w == $h)) {
		$adjusted_height = $h / $wm;
		$half_height = $adjusted_height / 2;
		$int_height = $half_height - $h_height;
		imagecopyresampled($dimg, $simg, 0 , -$int_height, 0, 0, $nw, $adjusted_height, $w, $h);
	} else {
		imagecopyresampled($dimg,$simg, 0, 0, 0, 0, $nw, $nh, $w, $h);
	}

	imagejpeg($dimg, $dest, 100);
}

foreach ($fileslist as $key => $value) {
	$files[$value] = array(date('d.m.Y H:i:s.', filemtime($pathimg.$value)), ceil(filesize($pathimg.$value)/1024) . ' Kbyte');
}

// Проверка и создание директории
if (!file_exists($pathcsv)) {
    mkdir($pathcsv);
}

$fopenw = fopen($pathcsv.$mycsv, 'w');

foreach ($files as $key => $value) {
    fputcsv($fopenw, array_merge([$key], $value));
}

fclose($fopenw);

$fopenr = fopen($pathcsv.$mycsv, 'r');

while (($data = fgetcsv($fopenr)) !== FALSE) {
	$num = count($data);
	for ($i=0; $i < $num; $i++) {
		if ($i == 0) {
			echo '<a href="'.$pathimg.$data[0].'">'.$data[0].'</a><br>';
		} else {
			echo $data[$i] . '<br>';
		}
	}
	echo '<br>';
}

?>